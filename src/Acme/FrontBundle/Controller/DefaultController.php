<?php

namespace Acme\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AcmeFrontBundle:Default:index.html.twig', array());
    }
    public function aboutAction()
    {
        return $this->render('AcmeFrontBundle:Default:onas.html.twig', array());
    }
    public function offerAction()
    {
        return $this->render('AcmeFrontBundle:Default:oferta.html.twig', array());
    }
    public function downloadAction()
    {
        return $this->render('AcmeFrontBundle:Default:dopobrania.html.twig', array());
    }
    public function contactAction()
    {
        return $this->render('AcmeFrontBundle:Default:zapytanie.html.twig', array());
    }
}
